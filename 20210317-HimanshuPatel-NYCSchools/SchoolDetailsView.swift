//
//  SchoolDetailsView.swift
//  20210317-HimanshuPatel-NYCSchools
//
//  Created by himanshu on 3/17/21.
//  Copyright © 2021 himanshu. All rights reserved.
//

import UIKit

class SchoolDetailsView: UIViewController {

    @IBOutlet weak var lblSchoolName: UILabel!
    @IBOutlet weak var lblSatTestTaker: UILabel!
    @IBOutlet weak var lblSatMath: UILabel!
    @IBOutlet weak var lblSatReading: UILabel!
    @IBOutlet weak var lblSatWriting: UILabel!
    

    
    let dCellReuseIdentifier = "dCellReuseIdentifier"

    var dbnparameter : String = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.title="Nyc School and Sat Deatils";

        dbnparameter=sigltoneData.instance.dbnparameter

        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        schoolSatApiCall();

    }
    //Api call for school and sat data
    func schoolSatApiCall(){

        self.showSpinner(onView: self.view)


        
       // DispatchQueue.global(qos: .userInitiated).async {


            let targetURL = URL(string:schoolSatURL + self.dbnparameter)

            var request = URLRequest(url: (targetURL)! as URL)
        request.httpMethod = "GET"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session2 = URLSession.shared
        let task = session2.dataTask(with: request, completionHandler: { data, response, error -> Void in
            print(response!)
            do {
                self.removeSpinner()

                let json = try JSONSerialization.jsonObject(with: data!,options: [])
                
                    
               
                if  let object = json as? [Any] {
                    DispatchQueue.main.async {
                        
                    for anItem in object as! [NSDictionary] {
                        if((anItem["school_name"]) != nil){
                            self.lblSchoolName.text = "School Name:" + (anItem["school_name"] as! String)
                        }
                        if((anItem["num_of_sat_test_takers"]) != nil){
                            self.lblSatTestTaker.text = "Test Taker:" + (anItem["num_of_sat_test_takers"] as! String)
                            
                        }
                        if((anItem["sat_math_avg_score"]) != nil){
                            self.lblSatMath.text = "Math Score:" + (anItem["sat_math_avg_score"] as! String)
                            
                        }
                        
                        if((anItem["sat_critical_reading_avg_score"]) != nil){
                            self.lblSatReading.text = "Math Score:" + (anItem["sat_critical_reading_avg_score"] as! String)
                            
                        }
                        
                        if((anItem["sat_writing_avg_score"]) != nil){
                            self.lblSatWriting.text = "Math Score:" + (anItem["sat_writing_avg_score"] as! String)
                            
                        }
                        
                        
                        
                        
                    }

                        
                    }
                    
                    
                } else {
                    print("JSON is invalid")
                }
                
                
            } catch {
                print("error")
            }
        })
        
        task.resume()
        
    }
    
    
   // }
    
    
}




