//
//  SchoolTableViewCell.swift
//  20210317-HimanshuPatel-NYCSchools
//
//  Created by himanshu on 3/17/21.
//  Copyright © 2021 himanshu. All rights reserved.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {
    
   
    @IBOutlet weak var lblSchoolName: UILabel!
    @IBOutlet weak var lblSchoolEmail: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var lblFaxNumber: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblWebsite: UILabel!
    @IBOutlet weak var lblBorough: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
