//
//  ViewController.swift
//  20210317-HimanshuPatel-NYCSchools
//
//  Created by himanshu on 3/17/21.
//  Copyright © 2021 himanshu. All rights reserved.
//

import UIKit

private var schoolData = [NSDictionary]()
class sigltoneData {
    
    //creates the instance and guarantees that it's unique
    static let instance = sigltoneData()
    
    private init() {
    }
    
    //creates the global variable
    var dbnparameter = ""
}
class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    
    let CellReuseIdentifier = "CellReuseIdentifier"

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.title="Nyc School List";
        self.tableView.register(UINib(nibName: String(describing: SchoolTableViewCell.self), bundle: nil), forCellReuseIdentifier: CellReuseIdentifier)
        self.tableView.rowHeight  = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 80
        self.tableView.dataSource = self
        self.tableView.delegate = self
        schoolListApiCall();

        
    }
    
    override func viewWillAppear(_ animated: Bool) {


        

    }
    //Api call for school data
    func schoolListApiCall(){
        

        self.showSpinner(onView: self.view)

        
//        DispatchQueue.global(qos: .userInitiated).async {

        var request = URLRequest(url:schoolListURL! as URL)
        request.httpMethod = "GET"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            print(response!)
            do {

                let json = try JSONSerialization.jsonObject(with: data!,options: [])

                self.removeSpinner()

                if  let object = json as? [Any] {

                    
                    for anItem in object as! [NSDictionary] {
                        
                        
                        
                        
                        schoolData.append(anItem)

                    }

                    DispatchQueue.main.async {

                        self.tableView.reloadData()
                        
                    }
                  

                
                    
                } else {
                    print("JSON is invalid")
                }
                
                
            } catch {
                print("error")
            }
        })
        
        task.resume()
    //}
}
    
    //Navigate to new detailview on row select
    func tableTapped() {
        self.performSegue(withIdentifier: "schoolDetailsView", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "schoolDetailsView" {
            
            if let indexPath = self.tableView.indexPathForSelectedRow {

                
                if ((schoolData[indexPath.row].value(forKey: "dbn")) != nil){

                    let controller = segue.destination as! SchoolDetailsView

                 controller.dbnparameter =
                    
                    (schoolData[indexPath.row].value(forKey: "dbn") as! String)

                }

            }
        }
    }

}

var vSpinner : UIView?

extension UIViewController {
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            vSpinner?.removeFromSuperview()
            vSpinner = nil
        }
    }
}


extension ViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier, for: indexPath) as! SchoolTableViewCell
        cell.accessoryType = .detailDisclosureButton;
        if ((schoolData[indexPath.row].value(forKey: "school_name")) != nil){
            cell.lblSchoolName.text = "Name:" + (schoolData[indexPath.row].value(forKey: "school_name") as! String)
            
        }
        if ((schoolData[indexPath.row].value(forKey: "school_email")) != nil){
            cell.lblSchoolEmail.text = "Email:" + (schoolData[indexPath.row].value(forKey: "school_email") as! String)

        }
        
        if ((schoolData[indexPath.row].value(forKey: "website")) != nil){
            cell.lblWebsite.text = "website:" + (schoolData[indexPath.row].value(forKey: "website") as! String)
            
        }
        
        if ((schoolData[indexPath.row].value(forKey: "fax_number")) != nil){
            cell.lblFaxNumber.text = "Fax:" + (schoolData[indexPath.row].value(forKey: "fax_number") as! String)
            
        }
        
        if ((schoolData[indexPath.row].value(forKey: "phone_number")) != nil){
            cell.lblPhoneNumber.text = "Phone:" + (schoolData[indexPath.row].value(forKey: "phone_number") as! String)
            
        }
        if ((schoolData[indexPath.row].value(forKey: "location")) != nil){
            cell.lblLocation.text = "Location:" + (schoolData[indexPath.row].value(forKey: "location") as! String)
            
        }
        if ((schoolData[indexPath.row].value(forKey: "borough")) != nil){
            cell.lblBorough.text = "Borough:" + (schoolData[indexPath.row].value(forKey: "borough") as! String)
            
        }
        
        return cell
    }
}

extension ViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
       
        sigltoneData.instance.dbnparameter = (schoolData[indexPath.row].value(forKey: "dbn") as! String)

        
        

        self.tableTapped()
        
    }


}

